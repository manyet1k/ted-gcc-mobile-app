import 'package:flutter/material.dart';
import 'package:ted_gcc_mobile_app/styles.dart';
import 'package:ted_gcc_mobile_app/main.dart';
import 'package:ted_gcc_mobile_app/about_us.dart';
class NavBar extends StatelessWidget {
  const NavBar({Key? key}) : super(key: key);

  void pageLoader(BuildContext context, int pageIndex){
    switch(pageIndex){
      case 0:
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const MyApp())
          );
        break;
      case 1:
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AboutUs())
          );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const UserAccountsDrawerHeader(accountName: Text(""), accountEmail: Text(""),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage("https://tedsakaryagcc.xyz/img/app.png"),
              )
            )
          ),
          ListTile(
            leading: const Icon(Icons.description),
            title: const Text("Articles", style: Styles.textDefault),
            onTap: () => pageLoader(context, 0)
          ),
          ListTile(
            leading: const Icon(Icons.people),
            title: const Text("About Us", style: Styles.textDefault),
            onTap: () => pageLoader(context, 1)
          ),
        ],
      ),
    );
  }
}
